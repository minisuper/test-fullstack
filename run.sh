#!/bin/bash
mkdir cakephp/logs
mkdir cakephp/tmp
sudo chmod -R 777 cackephp/logs
sudo chmod -R 777 cakephp/tmp
cd cakephp
composer install
cd ..
cd docker
docker-compose up -d
docker exec -it testfs-php-fpm /bin/bash ./bin/cake migrations migrate
