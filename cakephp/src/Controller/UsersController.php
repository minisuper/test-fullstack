<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Form\UserForm;
use Cake\Http\Client;

use Cake\Log\Log;
/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function register() {
        if ($this->request->is('get')) {
            $action = 'stepOne';
            $id = null;
            if ($this->request->query('user')) {
                $id = intval($this->request->query('user'));
                $user = $this->Users->get($id);
                if (!$user->isNew()) {
                    $step = $user->step();
                    switch ($step) {
                        case 2:
                            $action = 'stepTwo';
                            break;
                        case 3:
                            $action = 'stepThree';
                            break;
                        case 4:
                            $action = 'stepFour';
                            break;
                        default:
                            $action = 'stepOne';
                            break;
                    }
                }
            }
            return $this->redirect([
                'action' => $action,
                '?' => [
                    'user' => $id
                ]
            ]);

        }
    }

    public function stepOne() {
        $userForm = new UserForm();
        if ($this->request->is('post')) {
            $user = $this->Users->newEntity();
            $user = $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                return $this->redirect([
                    'action' => 'stepTwo',
                    '?' => [
                        'user' => $user->id
                    ]
                ]);
            } else {
                $this->Flash->error('Error creating account.');
            }
        }
        $this->set('user', $userForm);
    }

    public function stepTwo() {
        if (!$this->request->query('user')) {
            return $this->redirect([
                'action' => 'stepOne'
            ]);
        }
        $userForm = new UserForm();
        if ($this->request->is('post')) {
            $id = intval($this->request->query('user'));
            $user = $this->Users->get($id);
            $data = $this->request->getData();
            $user->street = $data['street'];
            $user->house_number = $data['house_number'];
            $user->zip_code = $data['zip_code'];
            $user->city = $data['city'];
            if ($this->Users->save($user)) {
                return $this->redirect([
                    'action' => 'stepThree',
                    '?' => [
                        'user' => $user->id
                    ]
                ]);
            } else {
                Log::debug(print_r($user, true));
                $this->Flash->error('Error creating account.');
            }
        }
        $this->set('user', $userForm);
    }

    public function stepThree() {
        if (!$this->request->query('user')) {
            return $this->redirect([
                'action' => 'stepOne'
            ]);
        }
        $userForm = new UserForm();
        if ($this->request->is('post')) {
            $id = intval($this->request->query('user'));
            $user = $this->Users->get($id);
            $data = $this->request->getData();
            $user->account_owner = $data['account_owner'];
            $user->iban = $data['iban'];
            // API Call //
            $http = new Client();
            $jsonData = [
                'customerId' => $user->id,
                'iban' => $user->iban,
                'owner' => $user->account_owner
            ];
            $response = $http->post(
                'https://37f32cl571.execute-api.eu-central-1.amazonaws.com/default/wunderfleet-recruiting-backend-dev-save-payment-data',
                json_encode($jsonData),
                [
                    'headers' => [
                        'Content-Type' => 'application/json'
                    ]
                ]
            );
            $res = json_decode($response->body);
            if ($response->getStatusCode() == 200) {
                $user->payment_data_id = $res->paymentDataId;
                if ($this->Users->save($user)) {
                    return $this->redirect([
                        'action' => 'stepFour',
                        '?' => [
                            'user' => $user->id
                        ]
                    ]);
                } else {
                    $this->Flash->error('Error creating account.');
                }
            } else {
                $this->Flash->error('Error creating account getting payment data id');
            }
        }
        $this->set('user', $userForm);
    }

    public function stepFour() {
        if (!$this->request->query('user')) {
            return $this->redirect([
                'action' => 'stepOne'
            ]);
        }
        $userForm = new UserForm();
        $id = intval($this->request->query('user'));
        $user = $this->Users->get($id);
        $this->set('paymentDataId', $user->payment_data_id);
    }
}
