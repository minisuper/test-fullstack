<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * User Entity
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $telephone
 * @property string $account_owner
 * @property string $iban
 */
class User extends Entity
{

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'first_name' => true,
        'last_name' => true,
        'telephone' => true,
        'account_owner' => true,
        'iban' => true
    ];


    public function step() {
        $step = 1;
        if (
            $this->first_name &&
            $this->last_name &&
            $this->telephone
        ) {
            $step = 2;
            if (
                $this->street &&
                $this->house_number &&
                $this->zip_code &&
                $this->city
            ) {
                $step = 3;
                if (
                    $this->account_owner &&
                    $this->iban &&
                    $this->payment_data_id
                ) {
                    $step = 4;
                }
            }
        }
        return $step;
    }
}
