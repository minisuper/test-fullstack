<?php
namespace App\Form;

use Cake\Form\Form;
use Cake\Form\Schema;
use Cake\Validation\Validator;

class UserForm extends Form
{

    protected function _buildSchema(Schema $schema)
    {
        return $schema->addField('first_name', 'string')
            ->addField('last_name', ['type' => 'string'])
            ->addField('telephone', ['type' => 'string'])
            ->addField('street', ['type' => 'string'])
            ->addField('house_number', ['type' => 'string'])
            ->addField('zip_code', ['type' => 'string'])
            ->addField('city', ['type' => 'string'])
            ->addField('account_owner', ['type' => 'string'])
            ->addField('iban', ['type' => 'string']);
    }

    protected function _buildValidator(Validator $validator)
    {
        $validator->add('first_name', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'First name is required'
            ])->add('last_name', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Last name is required'
            ])->add('telephone', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Telephone is required'
            ])->add('street', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Street is required'
            ])->add('telephone', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Telephone is required'
            ])->add('house_number', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'House number is required'
            ])->add('zip_code', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Zip code is required'
            ])->add('city', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'City is required'
            ])->add('account_owner', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Account Owner is required'
            ])->add('iban', 'length', [
                'rule' => ['minLength', 3],
                'message' => 'Iban is required'
            ]);

        return $validator;
    }

}
?>
