# Test

## Requirements
* docker
* docker-compose
* Composer installed globally: https://getcomposer.org/doc/00-intro.md#globally

## Installation
1. Clone this repo: ```git clone git@bitbucket.org:minisuper/test-fullstack.git ```
2. In other terminal run: ```chmod +x run.sh && ./run.sh```

If you want check the MySQL Database run: ```docker exec -it testfs-mysql /usr/bin/mysql -utestfsuser -p test-fs``` . The password is 123123123

In database we have an **user** table:
```
mysql> desc users;
+---------------+--------------+------+-----+---------+----------------+
| Field         | Type         | Null | Key | Default | Extra          |
+---------------+--------------+------+-----+---------+----------------+
| id            | int(11)      | NO   | PRI | NULL    | auto_increment |
| first_name    | varchar(255) | NO   |     | NULL    |                |
| last_name     | varchar(255) | NO   |     | NULL    |                |
| telephone     | varchar(255) | NO   |     | NULL    |                |
| account_owner | varchar(255) | YES  |     | NULL    |                |
| iban          | varchar(255) | YES  |     | NULL    |                |
| street        | varchar(255) | YES  |     | NULL    |                |
| house_number  | varchar(255) | YES  |     | NULL    |                |
| zip_code      | varchar(255) | YES  |     | NULL    |                |
| city          | varchar(255) | YES  |     | NULL    |                |
+---------------+--------------+------+-----+---------+----------------+

```

## Things that I could be done better
* The MySQL user/password could be take from .env file
* I could be use some Front end framework like VueJS in order to improve the UI.  
* The *?user* param is not safe. I can use cookie and sessions for check the steps of users.  
* The handling errors is very precarious.  
* The city could be take from another table.  
* The validation forms is very precarious.
